clc;
clear all;
close all;

%% Universal parameters for Cyber d40
stall_torque_d40 = 0.35; % [Nm] From datasheet
noload_speed_d40 = 5575*(pi/30); % [rad/s] From datasheet
V_peak_d40 = 48; % [V] Peak voltage, from datasheet
m_d40 = stall_torque_d40 / noload_speed_d40; % [Nm/rad/s] Calculated from datasheet values
I_m_d40 = 250*10^-8; % [kgm^2] Motor inertia, from datasheet
N_d40 = 20; % Gearing ratio chosen for the Cyber motors

B_d40 = 0.003 / (1000 * pi / 30); % [Nm/rad/s] Viscous friction. Assumed value, must be measured experimentally later
R_d40 = 5; % [ohm] Electrical resistance. Assumed value, must be measured experimentally later

Kv_d40 = V_peak_d40 / noload_speed_d40; % Voltage constant
Kt_d40 = Kv_d40; % Torque constant, equal to the voltage constant in SI-units

Km_d40 = Kt_d40 / sqrt(R_d40); % [Nm/sqrt(W)] Motor constant, calculated value
%% Parameters for the Pitch-axis (Cyber d40)
I_L_pitch = (1.923*10^-2); % [kgm^2] Load inertia, from Solidworks model

T_d40_pitch = motor_time_constant(I_m_d40, I_L_pitch, m_d40, B_d40, N_d40); % Time constant of the motor
%% Parameters for the Yaw-axis (2x Cyber d40)
I_L_yaw = (6*10^-2); % [kgm^2] % Load inertia, from Solidworks model, divided by two because it is driven by two motors

T_d40_yaw = motor_time_constant(I_m_d40, I_L_yaw, m_d40, B_d40, N_d40); % Time constant of the motor
%% Parameters for the X-axis (AKMH32E)
stall_torque_x = 1.77; % [Nm] From datasheet
noload_speed_x = 8000*(pi/30); % [rad/s] From datasheet
m_x = stall_torque_x / noload_speed_x; % [Nm/rad/s] Calculated value
I_m_x = 5.9*10^-5; % [kgm^2] Motor inertia (rotor), from datasheet
B_x = 0.003 / (1000 * pi / 30); % [Nm/rad/s] Viscous friction, from datasheet
N_x = 5; % Gearing ratio for the X-axis motor

Km_x = 0.24; % Motor constant, from datasheet

I_L_x = 19 * (0.04)^2; % [kgm^2] Load inertia, from Solidworks model

T_x = motor_time_constant(I_m_x, I_L_x, m_x, B_x, N_x); % Time constant of the motor

%% Parameters for the Y-axis (2x AKMH52H)
stall_torque_y = 6.29; % [Nm] From datasheet
noload_speed_y = 6000 * (pi / 30); % [rad/s] From datasheet
m_y = stall_torque_y / noload_speed_y; % [Nm/rad/s] Calculated value
I_m_y = 6.22*10^-4; % [kgm^2] Motor inertia (rotor), from datasheet
B_y = 0.042 / (1000 * pi / 30); % [Nm/rad/s] Viscous friction, from datasheet
N_y = 5; % Gearing ratio for the Y-axis motors

Km_y = 0.76; % Motor constant, from datasheet

I_L_y = (65 * (0.04)^2) / 2; % [kgm^2] Load inertia, divided by two as it is being driven by two motors

T_y = motor_time_constant(I_m_y, I_L_y, m_y, B_y, N_y); % Time constant of the motor

%% Parameters for the Z-axis (4x AKMH64K)
stall_torque_z = 18.7; % [Nm] From datasheet
noload_speed_z = 6000 * (pi / 30); % [rad/s] From datasheet
m_z = stall_torque_z / noload_speed_z; % [Nm/rad/s] Calculated value
I_m_z = 3.16*10^-3; % [kgm^2] Motor inertia (rotor) from datasheet
B_z = 0.08 / (1000 * pi / 30); % [Nm/rad/s] Viscous friction, from datasheet
N_z = 5; % Gearing ratio for the Z-motors

Km_z = 1.6; % Motor constant, from datasheet

I_L_z = ((200* (0.06)^2) + 0.036) / 4; %   [kgm^2] Load inertia, divided by two as it is begin driven by four motors

T_z = motor_time_constant(I_m_z, I_L_z, m_z, B_z, N_z); % Time constant of the motors

%% Function which calculates the motor time constant
function a = motor_time_constant(I_m, I_L, m, B, N)
a = (I_m + (I_L)/N^2) / (m + B);
end