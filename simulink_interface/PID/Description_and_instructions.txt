Matlab/Simulink can be bought from: https://se.mathworks.com/
Version R2018a was used in this project.
To be able to run the simulation with ROS subscriber and publisher blocks the "Simulink Robotics System Toolbox" must be downloaded from: https://se.mathworks.com/products/robotics.html

Description:
A Matlab script was written "Motorparameters_RoboTrim.m" where motorparameters are declared and calculations with these are done.
These variables are used in two Simulink-simulations.
The first is "RoboTrim_PID_tuning.slx". This simulation was used as a stand-alone simulation for tuning the PID-values.
The second is "RoboTrim_PID_ROS.slx". This simulation is used together with the ROS program to simulate a complete system.

Instructions:
RoboTrim_PID_tuning:
The Matlab script is being run by: Editor, Run. This declares the variables.
After this, the Simulink diagram can be run. 
The response of each motor can then be viewed by double clicking on each scope respectively.
PID values can be changed in each individual PID-block. 

RoboTrim_PID_ROS:
Initiate the ROS simulation.
The Matlab script is being run by: Editor, Run. This declares the variables.
After this, the Simulink diagram can be run.
The length of the simulation can be changed under "Simulation stop time". By inputting "inf" the simulation can run until being stopped manually.
The diagram with the ROS interface must be run together with ROS. This is described where the ROS source code is found.