#ifndef OBJECT_GENERATOR_H
#define OBJECT_GENERATOR_H

#include <ros/ros.h> /**< Header file for the roscpp implementation of ROS. Provides client library to interface with ROS topics, services and parameters */
#include <custom_msgs/CameraOutput.h> /**< Custom message-definition header for CameraOutput.msg */
#include <gazebo_msgs/SpawnModel.h> /**< Service-definition header for spawning models in Gazebo */
#include <gazebo_msgs/DeleteModel.h> /**< Service-definition header for deleting models in Gazebo */
#include <gazebo_msgs/GetModelState.h> /**< Service-definition header for getting model states from Gazebo */
#include <gazebo_msgs/SetLinkState.h> /**< Service-definition header for setting model states in Gazebo */
#include <gazebo_msgs/GetLinkState.h> /**< Service-definition header for getting link states from Gazebo */
#include <gazebo_msgs/GetModelProperties.h> /**< Service-definition header for getting model properties from Gazebo */
#include <geometry_msgs/Pose.h> /**< Message-definition header for message specifying a link pose */
#include <vector>

/**
 * \class ObjectGenerator
 *
 * \brief Class responsible for the handling of the simulated environment i Gazebo.
 *
 * Incorporates functionality to spawn markers to the simulated environment in Gazebo.
 * Builds an environment around the model of RoboTrim in order to simulate a conveyor
 * belt that continously feeds markers representing the fish to cut. The markers are by
 * default set to spawn three spots per marker to comply with requirements given.
 *
 * The markers are approximated to 70cm in length and 30cm in width. The frequency of
 * spawning markers is set to 3Hz which means that there will be a difference between
 * spawns of about 0.33 seconds. The spawned marker models are travelling at a speed of
 * 1 m/s specified by the `model_push.cpp` plugin located in rt_model/src/plugins.
 */
class ObjectGenerator
{
public:
    /**
     * \brief Default constructor
     *
     * Initializes all service clients used in the object generator and parses
     * the URDF/SDF/Xacro file given by the path to file.
     */
    ObjectGenerator(ros::NodeHandle* nh);

    /**
     * \brief Default destructor
     */
    ~ObjectGenerator(){};

    /**
     * \brief Change the position of spots on the fish realtime. This needs to be
     * done through the LinkState in order to synchronize the movement.
     *
     * Takes model name and links to be moved and sets a new link state through
     * by sending a request to the set_link_state service in Gazebo.
     *
     * @param name The name of the model containing the links.
     * @param links The links to be moved given as a vector.
     */
    std::vector<gazebo_msgs::SetLinkState> changeLinkPosition(const std::string& name, const std::vector<std::string>& links);

    /**
     * \brief Initializes vector with ints representing names of models for markers
     * spawned in the simulation environment.
     *
     * Creates a vector of tuples that contain name of the model and a boolean
     * indicating if the model has been spawned or not.
     *
     * @param number The number of markers to spawn.
     */
    void initialize(const int& number);

    /**
     * \brief Parses the model given and splits it into subsets
     * depending on the number of spots each marker should have.
     *
     * Handles the reproduction of markers by utilizing ros::rate which
     * operates in Hz.
     *
     * @param links Vector of links in the given marker
     * @param name Name of the given marker
     */
    void markerHandler(const std::vector<std::string>& links, const std::string& name);

    /**
     * \brief Wrapper function to simulate the conveyor belt with markers
     * with randomly generated spots
     */
    void runConveyor();

    /**
     * \brief Spawns markers to the simulation environment.
     *
     * Fills a SpawnModel-request which is sent to the spawn_sdf_model
     * service in Gazebo.
     *
     * @param model_name Desired name of the marker-model.
     */
    void spawnMarker(const std::string& model_name);

    /**
     * \brief Determine the min value of three given. Used to determine
     * the position of spots in relation to the total length of the marker.
     *
     * @param x The x positions of spots to compare.
     */
    double find_min(const std::vector<double>& x);

    void cameraOutput(std::vector<gazebo_msgs::SetLinkState>& xy);

    /**
     * \brief Generate random floats that represent x, y position for spots.
     *
     * @param min Minimum value based on total marker size.
     * @param max Maximum value based on total marker size.
     */
    double generateRandomPos(const double& min, const double& max);

    /**
     * \brief Monitor the location of spots in the x-direction in order
     * to determine when they should be deleted from the simulation.
     *
     * Fills a temporary GetModelState msg which is used to request the
     * model state through the get_model_state service of Gazebo
     *
     * @param model_name
     */
    double monitorSpots(const std::string& model_name);

    /**
     * \brief Returns all links of a given SDF or URDF model that is used as
     * a marker in the simulation.
     *
     * @params name Name of the model to get links from.
     */
    std::vector<std::string> getLinks(const std::string& name);


private:
    ros::NodeHandle* node_ptr_;
    ros::ServiceClient srv_gazebo_spawn_; /**< Service client used to spawn models to Gazebo */
    ros::ServiceClient srv_get_link_state_; /**< Service client used to get link states from Gazebo */
    ros::ServiceClient srv_get_model_properties_; /**< Service client used to get model properties from Gazebo */
    ros::ServiceClient srv_monitor_client_; /**< Service client used to monitor markers in Gazebo */
    ros::ServiceClient srv_set_link_state_; /**< Service client used to set link states in Gazebo */
    ros::Publisher pub_camera_output_; /**< Publisher that publishes the simulated camera output */
    gazebo_msgs::GetModelProperties msg_get_model_properties_; /**< Message variable used for services */
    gazebo_msgs::GetLinkState msg_get_link_state_; /**< Message variable used for services */
    gazebo_msgs::SpawnModel msg_model_; /**< Message variable used for services */
    gazebo_msgs::SetLinkState msg_set_link_state_; /**< Message variable used for services */

    std::vector<std::string> models_; /**< Variable used to store model names */
    std::vector<std::tuple<std::string, bool>> model_control_; /**< Vector used to store model-name and spawn-status */

};

#endif
