#include <ros/ros.h>
#include <geometry_msgs/Pose.h>

#include "ObjectGenerator.h"

/**
 * Main execution file to spawn markers in the simulation environment.
 */
int main(int argc, char** argv)
{
    ros::init(argc, argv, "fish_generator");

    // Initialize ROS-clock
    ros::Time::init();
    ros::NodeHandle nh;
    ObjectGenerator object_gen(&nh);
    ros::Rate r(50);

    object_gen.initialize(1);

    while(ros::ok())
    {
        // Run wrapper function
        object_gen.runConveyor();
        ros::spinOnce();
        r.sleep();
    }
}
