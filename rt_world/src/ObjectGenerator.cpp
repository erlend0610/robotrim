#include "ObjectGenerator.h"
#include <iostream>
#include <fstream>
#include <std_msgs/Float32MultiArray.h>
#include <custom_msgs/CameraOutput.h>
#include <custom_msgs/EulerAngles.h>

#define X_MIN -2.175f
#define X_MAX -1.475f
#define Y_MIN -1.100f
#define Y_MAX -0.850f
#define Z_MIN 0.0f
#define Z_MAX 0.098f
#define X_INIT -1.5f

ObjectGenerator::ObjectGenerator(ros::NodeHandle* nh)
{
    // Node pointer for ROS
    node_ptr_ = nh;

    // Service client used to set link states
    srv_set_link_state_ = node_ptr_ ->
        serviceClient<gazebo_msgs::SetLinkState>("/gazebo/set_link_state");

    // Service client used to get link states
    srv_get_link_state_ = node_ptr_ ->
        serviceClient<gazebo_msgs::GetLinkState>("/gazebo/get_link_state");

    // Service client used to get model properties
    srv_get_model_properties_ = node_ptr_ ->
        serviceClient<gazebo_msgs::GetModelProperties>("/gazebo/get_model_properties");

    // Service client used to spawn models into the simulation environment
    srv_gazebo_spawn_ = node_ptr_ ->
        serviceClient<gazebo_msgs::SpawnModel>("/gazebo/spawn_sdf_model", true);

    // Service client to get model states of models in the simulation
    srv_monitor_client_ = node_ptr_ ->
        serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state", true);

    pub_camera_output_ = node_ptr_ ->
        advertise<custom_msgs::CameraOutput>("/robotrim/mock_cam_output", 100);

    // Location of SDF file used for markers
    std::ifstream file("src/robotrim/rt_world/urdf/spot.sdf");

    // Read content from SDF file
    for(std::string line; std::getline(file, line);) {
        msg_model_.request.model_xml+=line;
    }

    // Close file
    file.close();

}

std::vector<gazebo_msgs::SetLinkState> ObjectGenerator::changeLinkPosition(const std::string& name, const std::vector<std::string>& links)
{
    std::vector<gazebo_msgs::SetLinkState> temp_xy;

    // Iterate over the links given by the size of vector links
    for (int i = 0; i < links.size(); ++i)
    {
        // Get link by name
        msg_set_link_state_.request.link_state.link_name = ("%s::%s", name.c_str(), links[i].c_str());

        // Generate random initial x and y values for pose
        msg_set_link_state_.request.link_state.pose.position.x = generateRandomPos(X_MIN, X_MAX);
        msg_set_link_state_.request.link_state.pose.position.y = generateRandomPos(Y_MIN, Y_MAX);
        msg_set_link_state_.request.link_state.pose.position.z = generateRandomPos(0.642857, 0.68);
        temp_xy.push_back(msg_set_link_state_);

        // Initial reference frame
        msg_set_link_state_.request.link_state.reference_frame = "world";

        // Set link state
        srv_set_link_state_.call(msg_set_link_state_);
    }

    return temp_xy;
}

void ObjectGenerator::initialize(const int& number)
{
    // Initialize the model_control vector with the given amount of models
    for (int i = 0; i < number; ++i)
    {
        // Push back into vector
        models_.push_back(std::to_string(i));

        // Initialize tuple with i as name and spawn status as false
        model_control_.push_back(std::make_tuple(std::to_string(i), false));
    }
}

void ObjectGenerator::runConveyor()
{
    std::vector<std::string> temp_links;
    std::string temp_name;
    bool temp_spawned;

    // Iterate over all the marker models in the environment
    for (int i = 0; i < model_control_.size(); ++i)
    {
        // Get values from the tuple-vector
        temp_name = std::get<0>(model_control_[i]);
        temp_spawned = std::get<1>(model_control_[i]);

        // Spawn model if not spawned
        if (!temp_spawned)
        {
            // Generate marker
            spawnMarker(temp_name);

            // Get links from model based on model name
            temp_links = getLinks(temp_name);

            // Make sure spots on marker are given random placement
            markerHandler(temp_links, temp_name);

            // Set to spawned
            std::get<1>(model_control_[i]) = true;
        } else {
            // Set link-vector to the response of service
            temp_links = getLinks(temp_name);

            // Handle regeneration of marker and place randomly
            markerHandler(temp_links, temp_name);
        }
    }
}

void ObjectGenerator::markerHandler(const std::vector<std::string>& links, const std::string& name)
{
    std::vector<std::string> temp_grouped_links;
    std::vector<gazebo_msgs::SetLinkState> temp_xy;
    int temp_counter = 0;
    // Number to divide the total number of spawned spots in the simulation
    int temp_divider = 3;
    // Spawn rate given in Hz (1 / t)
    ros::Rate temp_r(3.0);

    // Outer loop divides the links of the model into groups
    for (int i = 0; i < temp_divider; ++i)
    {
        temp_grouped_links = {};

        // Inner loop pushes links to the vector used for reproduction
        for (int j = 0; j < links.size() / temp_divider; ++j)
        {
            // Push back to vector
            temp_grouped_links.push_back(links[temp_counter]);

            // Sleep in order to reproduce one fish per second
            temp_r.sleep();
            ++temp_counter;
        }

        // Randomize spot spawn on marker
        temp_xy = changeLinkPosition(name, temp_grouped_links);
        cameraOutput(temp_xy);
    }
}

void ObjectGenerator::cameraOutput(std::vector<gazebo_msgs::SetLinkState>& xy)
{
    float temp_z, temp_a, temp_b, temp_c;
    custom_msgs::EulerAngles euler;
    custom_msgs::CameraOutput mock_output;

    for (int i = 0; i < xy.size(); ++i)
    {
        // Generate random positions for z, a, b and c within constraints given.
        temp_z = generateRandomPos(Z_MIN, Z_MAX);
        temp_a = generateRandomPos(0, 0.49525);
        temp_b = generateRandomPos(0, 0.49525);
        temp_c = generateRandomPos(0, 2 * M_PI);

        // Call GetLinkState-service to get current link states of the marker-model
        euler.x = xy[i].request.link_state.pose.position.x;
        euler.y = xy[i].request.link_state.pose.position.y;
        euler.z = xy[i].request.link_state.pose.position.z;

        // Set the values randomly generated for abc to the fields of the EulerAngle message
        euler.a = temp_a;
        euler.b = temp_b;
        euler.c = temp_c;

        // Push data in euler-variable to the CameraOutput message to be published
        mock_output.data.push_back(euler);
    }

    // Set timestamp
    mock_output.header.stamp = ros::Time::now();

    // Publish
    pub_camera_output_.publish(mock_output);
}

std::vector<std::string> ObjectGenerator::getLinks(const std::string& name)
{
    std::vector<std::string> temp_links;

    // Set name in request
    msg_get_model_properties_.request.model_name = name;

    // Call model properties with the model properties
    srv_get_model_properties_.call(msg_get_model_properties_);

    // Set the response from the service-call to temp-links
    temp_links = msg_get_model_properties_.response.body_names;

    return temp_links;
}

void ObjectGenerator::spawnMarker(const std::string& model_name)
{
    // Set name in request and generate initial x to initial reference frame
    msg_model_.request.model_name = model_name;
    msg_model_.request.initial_pose.position.x = X_INIT;

    // Call SpawnModel-service from Gazebo to spawn to simulation environment
    srv_gazebo_spawn_.call(msg_model_);
}

double ObjectGenerator::generateRandomPos(const double& min, const double& max)
{
    std::random_device temp_rd;
    std::mt19937 temp_gen(temp_rd());
    std::uniform_real_distribution<> temp_dis(min, max);

    return temp_dis(temp_gen);
}

double ObjectGenerator::monitorSpots(const std::string& model_name)
{
    gazebo_msgs::GetModelState temp_model_state;

    // Name of model to monitor and reference frame
    temp_model_state.request.model_name = model_name;

    // Call service to get model state from the simulation in Gazebo
    srv_monitor_client_.call(temp_model_state);

    return temp_model_state.response.pose.position.x;
}

double ObjectGenerator::find_min(const std::vector<double>& x)
{
    if ((x[0] >= x[1]) && (x[0] >= x[2]))
        return x[0];
    else if ((x[1] >= x[0]) && (x[1] >= x[2]))
        return x[1];
    else
        return x[2];
}
