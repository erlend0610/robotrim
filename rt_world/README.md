# rt_world

Package that contains the environment for simulation. Uses the model from the rt_model package
in Gazebo 7.0 to provide a visualization of the controller and it's dependent services and
publishers.

### Structure

* rt_world/
    * src/
        * ObjectGenerator.cpp
        * ObjectGenerator.h
        * ObjectHandler.cpp
    * launch/
        * rt_simulation.launch
    * urdf/
        * spot.sdf
    * worlds/
        * rt_simulation.world
    * CMakeLists.txt
    * package.xml

#### src/ObjectGenerator.cpp

Contains the source code that is responsible for handling the generation and respawning of
markers representing fish in the Gazebo simulation environment. The markers spawned are models
from urdf/spot.sdf. The generator spawns the markers by utilizing different services for the
Gazebo environment. The area to spawn inside is specified by the defined variables at the top
of the source code.

#### src/ObjectGenerator.h

Standard header file.

#### src/ObjectHandler.cpp

File that acts as a wrapper for rt_world and runs the fish_generator to spawn markers in Gazebo.

#### launch/rt_simulation.launch

Launch file to launch rt_simulation.world

#### urdf/spot.sdf

The model of the marker/spot is modelled as a simple red circle in SDF (model format).

#### worlds/rt_simulation.world

Simple world file included in rt_simulation.launch

### Usage

This package is implemented in the launch files specified in the rt_model package.
