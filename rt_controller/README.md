# rt_controller

Package that contains the program for controlling RoboTrim. 

### Structure

* rt_controller/
    * src/
        * Controller.cpp
        * Controller.h
        * ControllerSim.cpp
        * controller_main.cpp
    * CMakeLists.txt
    * package.xml

#### Controller.cpp

Source code for the controller without the interface to the Simulink environment simulating the electrical subsystem. 
Receives a JointTrajectory-message from the kinematic-service and publishes to the simulation environment in Gazebo.

#### Controller.h

Header file shared by Controller.cpp and ControllerSim.cpp

#### ControllerSim.cpp

Source code for the controller for usage with the Simulink environment. The major change from Controller.cpp is that the 
JointTrajectory-message received in the controller from the kinematic service is unwrapped in the controller. The controller
further synchronizes the publishing of positions for the X, Y, Z, pitch and yaw axis and publishes to Simulink at a rate of
100Hz.


#### controller_main.cpp

File that acts as a wrapper for rt_controller and runs the controller node in the ROS-network.

### Usage

To run as a separate node:

###### Navigate to the catkin workspace

    cd ~/rt_ws

###### Run the controller with rosrun

    rosrun rt_controller controller
