#include "Controller.h"
#include <std_msgs/Float64.h>
#include <custom_msgs/JointConfiguration.h>
#include <custom_msgs/KinematicService.h>
#include <custom_msgs/PlannerService.h>

Controller::Controller(ros::NodeHandle* nh)
{
    node_ptr_ = nh;

    /**
     * Initialize publishers, subscribers and services used in the Controller class.
     * Publishers and subscribers operate at a frequency of 100 Hz.
     */
    camera_sub_ = nh -> subscribe("mock_cam_output", 100, &Controller::cameraCallback, this);
    trajectory_pub_ = nh -> advertise<trajectory_msgs::JointTrajectory>("/robotrim/position_trajectory_controller/command", 100);
    x_pub_ = nh -> advertise<std_msgs::Float64>("/robotrim/x_axis/command", 100);
    y_pub_ = nh -> advertise<std_msgs::Float64>("/robotrim/y_axis/command", 100);
    z_pub_ = nh -> advertise<std_msgs::Float64>("/robotrim/z_axis/command", 100);
    yaw_pub_ = nh -> advertise<std_msgs::Float64>("/robotrim/yaw/command", 100);
    pitch_pub_ = nh -> advertise<std_msgs::Float64>("/robotrim/pitch/command", 100);

    /**
     * Initialize service clients for plannerService and kinematicService
     */
    joint_client_ = nh -> serviceClient<custom_msgs::KinematicService>("kinematic_service");
    planner_client_ = nh -> serviceClient<custom_msgs::PlannerService>("planning_service");

    is_init_ = false;
    counter_ = 0;
    ROS_INFO("Controller initialized");
}

void Controller::cameraCallback(const custom_msgs::CameraOutput& camera_output)
{
    // Set class variable to the variable received from "mock_cam_output"-subscriber
    from_camera_ = camera_output;
    // Update the current timestamp
    current_stamp_ = from_camera_.header.stamp;
}

void Controller::controllerWrapper()
{
    // Check if the controller has been initiated
    if (is_init_)
    {
        // Set the data point-data for positions to the class-variables for joints
        x_pos_.data = current_.points[counter_].positions[0];
        y_pos_.data = current_.points[counter_].positions[1];
        z_pos_.data = current_.points[counter_].positions[2];
        yaw_pos_.data = current_.points[counter_].positions[3];
        pitch_pos_.data = current_.points[counter_].positions[4];

        // Publish positional data
        x_pub_.publish(x_pos_);
        y_pub_.publish(y_pos_);
        z_pub_.publish(z_pos_);
        yaw_pub_.publish(yaw_pos_);
        pitch_pub_.publish(pitch_pos_);

        /**
         * Check if the timestamp in the current position-data object is lesser than the current
         * time.
         */
        if ((global_timer_ + current_.points[counter_].time_from_start) <= ros::Time::now())
        {
            if ((counter_ + 1) < (current_.points.size()))
            {
                ROS_INFO_STREAM("Incremented");
                ++counter_;
            } else {
                global_timer_ = global_timer_ + current_.points[counter_].time_from_start;
                counter_ = 0;
            }
        }
    }

    /**
     * If the queue for trajectories is not empty and the first item in queue has a timestamp lesser
     * than the current time. The positional data is published to topics for each joint at the rate
     * specified by the publisher.
     */
    if (!trajectory_queue_.empty() && trajectory_queue_.front().header.stamp <= ros::Time::now())
    {
        current_ = trajectory_queue_.front();
        trajectory_queue_.pop_front();
        if (!is_init_)
        {
            x_pos_.data = current_.points[counter_].positions[0];
            y_pos_.data = current_.points[counter_].positions[1];
            z_pos_.data = current_.points[counter_].positions[2];
            yaw_pos_.data = current_.points[counter_].positions[3];
            pitch_pos_.data = current_.points[counter_].positions[4];
            x_pub_.publish(x_pos_);
            y_pub_.publish(y_pos_);
            z_pub_.publish(z_pos_);
            yaw_pub_.publish(yaw_pos_);
            pitch_pub_.publish(pitch_pos_);
            global_timer_ = current_.header.stamp;
            is_init_ = true;
        }
    }
    if (current_stamp_ != last_unique_stamp_)
    {
        plan_srv_.request.Request = from_camera_;
        if (planner_client_.call(plan_srv_))
        {
            // Request to send to the kinematic server is set to the message from the camera node.
            kin_srv_.request.Request = plan_srv_.response.Response;
        }

    }
    // Check if the service-call was successful.
    if (joint_client_.call(kin_srv_))
    {
        // trajectory_pub_.publish(kin_srv_.response.Response);
        if (kin_srv_.response.Response.header.stamp.toSec() != 0)
            trajectory_queue_.push_back(kin_srv_.response.Response);

        last_unique_stamp_ = from_camera_.header.stamp;
    }
}

