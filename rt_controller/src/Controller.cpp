#include "Controller.h"
#include <std_msgs/Float64.h>
#include <custom_msgs/JointConfiguration.h>
#include <custom_msgs/KinematicService.h>
#include <custom_msgs/PlannerService.h>

Controller::Controller(ros::NodeHandle* nh)
{
    node_ptr_ = nh;

    /**
     * Initialize publishers, subscribers and services used in the Controller class.
     * Publishers and subscribers operate at a frequency of 100 Hz.
     */
    camera_sub_ = nh -> subscribe("/robotrim/mock_cam_output", 100, &Controller::cameraCallback, this);
    joint_client_ = nh -> serviceClient<custom_msgs::KinematicService>("/robotrim/kinematic_service");
    planner_client_ = nh -> serviceClient<custom_msgs::PlannerService>("/robotrim/planning_service");
    trajectory_pub_ = nh -> advertise<trajectory_msgs::JointTrajectory>("/robotrim/position_trajectory_controller/command", 100);
    ROS_INFO("Controller initialized");
}

void Controller::cameraCallback(const custom_msgs::CameraOutput &camera_output)
{
    // Update class camera-variable with the argument passed to the callback
    from_camera_ = camera_output;

    // Update timestamp in class variable
    current_stamp_ = from_camera_.header.stamp;
}

void Controller::controllerWrapper()
{
    if (current_stamp_ != last_unique_stamp_)
    {
        // Update the request 
        plan_srv_.request.Request = from_camera_;
        if (planner_client_.call(plan_srv_))
        {
            // Request to send to the kinematic server is set to the message from the camera node.
            kin_srv_.request.Request = plan_srv_.response.Response;
        }

        // Checks if the service-call was successful.
        if (joint_client_.call(kin_srv_))
        {
            // Publish the response from the kinematic service-call
            trajectory_pub_.publish(kin_srv_.response.Response);

            // Update the time stamp
            last_unique_stamp_ = from_camera_.header.stamp;
        }
    }
}
