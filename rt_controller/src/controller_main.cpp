#include <ros/ros.h>
#include "Controller.h"
#include <custom_msgs/CameraOutput.h>
#include <custom_msgs/JointConfiguration.h>
#include <custom_msgs/KinematicService.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, "rt_controller");
    ros::NodeHandle nh;
    Controller controller(&nh);

    // Spin ROS node with the wrapper function for the controller
    while(ros::ok())
    {
        controller.controllerWrapper();
        ros::spinOnce();
    }
}
