#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <ros/ros.h> /**< Header file for the roscpp implementation of ROS. Provides client library to interface iwth ROS topics, services and parameters */
#include <std_msgs/Float64.h> /**< Standard message-definition header */
#include <custom_msgs/CameraOutput.h> /**< Custom message-definition header for CameraOutput.msg */
#include <custom_msgs/KinematicService.h> /**< Custom service-definition header for KinematicService.srv */
#include <custom_msgs/PlannerService.h> /**< Custom service-definition header for PlannerService.srv */

/**
 * \class Controller
 *
 * \brief Main controller in the ROS-network for RoboTrim.
 *
 * Acts as a middle point for all communication in the ROS-network. The controller
 * receives a camera published by a camera node, in the simulation this message is
 * passed by a camera mock. Further it forwards the received message from the camera
 * to the path planner which finds a trajectory between the points in received for
 * the end-effector to follow. The trajectory is passed back as a service-response
 * to the controller. The message containing the desired end-effector poses is sent
 * in a request to the kinematic-service. The response from the service contains a
 * JointTrajectory-message with positional data for all joints for all poses
 * specified in the request-message sent from the controller.
 *
 * The joint-positions are synchronized in terms of start-time specified in the
 * JointTrajectory.msg and the time_from_start variable of the different points
 * of the path.
 */
class Controller
{
    public:

        /**
         * Standard constructor
         *
         * @param nh Pointer to NodeHandle object used within ROS
         */
        Controller(ros::NodeHandle* nh);

        ~Controller(){};

        /**
         * The function that is called when the controller gets a hit on the subscription
         * from the camera. At the moment, the only function the cameraCallback has is to
         * set the class variable equal to what is sent from the camera.
         *
         * @param camera_output CameraOuput variable containing the positions of markers.
         */
        void cameraCallback(const custom_msgs::CameraOutput &camera_output);

        /**
         * \brief Wrapper function for Controller used in the controller_main
         *
         * Wrapper function that acts as the main function for the controller.
         */
        void controllerWrapper();



    private:

        ros::NodeHandle* node_ptr_;
        ros::ServiceServer kinematic_service_; /**< Service used to get kinematic solutions for cartesian coordinates. */
        ros::Subscriber camera_sub_; /**< Subscriber that subscribes to the camera node in the system. */
        ros::ServiceClient joint_client_; /**< Client for the kinematic service which handles joint configuration. */
        ros::ServiceClient planner_client_; /**< Client for the planner service which handles the path planning. */
        ros::Publisher x_pub_; /**< Publisher to publish joint positions to the topic for the x-axis. */
        ros::Publisher y_pub_; /**< Publisher to publish joint positions to the topic for the y-axis. */
        ros::Publisher z_pub_; /**< Publisher to publish joint positions to the topic for the z-axis. */
        ros::Publisher pitch_pub_; /**< Publisher to publish joint positions to the topic for the pitch */
        ros::Publisher yaw_pub_; /**< Publisher to publish joint positions to the topic for yaw */
        ros::Publisher trajectory_pub_; /**< Publisher to publish joint positions to the topic for yaw */

        std_msgs::Float64 x_pos_, y_pos_, z_pos_, yaw_pos_, pitch_pos_; /**< Used to set values for joint positions */
        ros::Time global_timer_; /**< Global timer used to time trajectory points */
        int counter_; /**< Counter that is used for indexing the current trajectory response */
        bool is_init_; /**< Indicates if the controller is initialized and has received a trajectory response */
        std::list<trajectory_msgs::JointTrajectory> trajectory_queue_; /**< Stores JointTrajectory objects received from trajectory responses */

        ros::Time last_unique_stamp_; /**< Used to ensure uniqueness when passing messages */
        ros::Time current_stamp_; /**< Used to ensure uniqueness when passing messages */

        trajectory_msgs::JointTrajectory current_; /**< Stores the current JointTrajectory received from a trajectory response */
        custom_msgs::CameraOutput from_camera_; /**< Variable used for interception of messages passed from the camera node. Contents is forwarded to path planner which calculates path based on coordinates given. */
        custom_msgs::CameraOutput from_planner_; /**< Variable used for interception of messages passed from the planner node. Contents is forwarded to the kinematics node which returns the joint configurations for the path.*/
        custom_msgs::PlannerService plan_srv_; /**< Variable used when prompting the PlannerService for a path plan. */
        custom_msgs::KinematicService kin_srv_; /**< Variable used when prompting the KinematicService for kinematics of a plan. */
};

// end of class Controller
#endif
