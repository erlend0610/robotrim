# custom_msgs

Package that contains all custom message-definition (.msg) and service-definition (.srv) files.

### Structure

* custom_msgs/
    * msg/
        * CameraOutput.msg
        * EulerAngles.msg
        * JointConfiguration.msg
    * srv/
        * KinematicService.srv
        * PlannerService.srv
    * CMakeLists.txt
    * package.xml

#### msg/CameraOutput.msg

Mesage-definition for the message-type used for coordinates from the camera-mock.

    Header header // Header message containing timestamp used to indicate detection time
    EulerAngles[] data // Contains cartesian xyz and abc for rotation about axes

#### msg/EulerAngles.msg

Message-definition for the message structure contained in the array-objects of the data variable
in CameraOutput.msg.

    float32 x // Cartesian X
    float32 y // Cartesian Y
    float32 z // Cartesian Z
    float32 a // Rotation about X
    float32 b // Rotation about Y
    float32 c // Rotation about Z

#### msg/JointConfiguration.msg

Message-definition for joint-configurations.

float32 x // Value for the X-axis joint
float32 y // Value for the Y-axis joint
float32 z // Value for the Z-axis joint
float32 yaw // Value for the joint for yaw
float32 pitch // Valie for the joint for pitch

#### srv/KinematicService.srv

Service-definition file for the kinematic service in rt_kinematics.

Gets a CameraOutput request from the controller. Sends a JointTrajectory response.

#### srv/PlannerService.srv

Service-definition file for the planner service in rt_planner

Gets a CameraOutput request from the controller. Sends a CameraOutput response.

### Usage

In order to use custom messages defined in custom_msgs, the following needs to be included in
CMakeLists.txt of the package to import custom messages:

    find_package(
        message_generation
        custom_msgs
    )

    catkin_package(
        DEPENDS custom_msgs
    )

    add_dependencies(PACKAGE_NAME custom_msgs_generate_messages_cpp)
