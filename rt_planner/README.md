# rt_planner

Package that contains the service-node responsible for handling the path-planning the in the ROS-network.
Interfaces with the *controller* node from the rt_controller package by providing a service specified by
KinematicService.srv. The service takes requests that are specified as a CameraOutput.msg and sends a
response in the form of JointTrajectory.msg.

### Structure

* rt_planner/
    * src/
        * Planner.cpp
        * Planner.h
        * planner_main.cpp
    * CMakeLists.txt
    * package.xml

#### src/Planner.cpp

The path planned is based in the markers spawned in the simulation which simulates the spawning of
fish with three randomly placed spots contained in the area specified in rt_world/ObjectGenerator.cpp.

#### src/Planner.h

Standard header file

#### src/planner_main.cpp

Specified as the executable file in *CMakeLists.txt*. Runs the planner-node to provide the service specified
in Planner.cpp.

### Usage

To run as a separate node:

#### Navigate to the catkin workspace:

    cd ~/rt_ws

#### Run the node with `rosrun`:

    rosrun rt_planner planner

