#include <ros/ros.h>
#include "Planner.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "rt_planner");
    ros::NodeHandle nh;
    Planner planner(&nh);

    while(ros::ok())
    {
        ros::spin();
    }
}
