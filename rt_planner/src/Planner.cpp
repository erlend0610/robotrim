#include "Planner.h"

bool sortByX(const custom_msgs::EulerAngles &a, const custom_msgs::EulerAngles &b)
{
    return (a.x > b.x);
}

Planner::Planner(ros::NodeHandle* nh)
{
    node_ptr_ = nh;

    planner_service_ = node_ptr_ -> advertiseService("/robotrim/planning_service", &Planner::plannerServiceCallback, this);
    cut_length_ = 0.025f;
    x_ee_min_ = -0.020730f;
}

/**
 *  Can receive up from one to three spots
 *  Each spot is to be cut 5cm which corresponds to a cutting duration of 0.05s if the travelling speed
 *  of the fish is 1 m/s. The manipulator needs to lift linearly along Z in between each spot in order
 *  to not collide with the fish and cut undesired amounts.
 */
bool Planner::plannerServiceCallback(custom_msgs::PlannerService::Request &req,
        custom_msgs::PlannerService::Response &res)
{
    response_.Response = req.Request;
    sortPositions();
    assertViaPoints();
    res.Response.data.resize(response_.Response.data.size());
    res.Response = response_.Response;
}

void Planner::sortPositions()
{
    std::sort(response_.Response.data.begin(), response_.Response.data.end(), sortByX);
}

void Planner::assertViaPoints()
{
    float via_x, via_y, via_z; // Variables used to store values for viapoints in xyz
    float last_x; // The last x-value
    float x_diff; // The difference between two x-values
    float x_val; // Corrected x-value adjusted for difference between spots
    float time = 0.0f; // Stores the time stamps for a given sequence
    float intermediate_time; // Time between spots in a given sequence
    float correction; // Stores a correctional value for x
    float difference; // Stores difference in time between spots in terms of x-positions
    float x_real; // The real x-value adjusted for the difference to workspace and intermediate time
    ros::Time header_stamp; // The original time stamp from the response
    custom_msgs::CameraOutput placeholder; // Dummy variable to store temporary position data
    custom_msgs::EulerAngles cut_start; // Start position for a cut
    custom_msgs::EulerAngles cut_end; // End position for a cut
    std::vector<float> angles; // Angles used to determine cut direction
    std::vector<custom_msgs::EulerAngles> points; // Contains all trajectory points for a cutting sequence

    response_size_ = response_.Response.data.size();
    alloted_travel_time_ = calculateTotalTravelTime(response_size_);
    header_stamp = response_.Response.header.stamp;
    intermediate_time = calcIntermediateTime();

    /*
     * Iterate through the response data in order to set the desired trajectory
     * points for the robot corresponding to the positional data from xyzabc
     */
    for (int i = 0; i < response_.Response.data.size(); ++i)
    {
        if (i == 0)
        {
            x_diff = calcSpotDiff(response_.Response.data[i].x, x_ee_min_);
            x_val = response_.Response.data[i].x + x_diff;
            if (response_.Response.header.seq == 0)
            {
                time = time + x_diff;
            } else {
                response_.Response.header.stamp = finished_at_;
                difference = finished_at_.toSec() - header_stamp.toSec();
                x_real = response_.Response.data[i].x + difference + intermediate_time;
                correction = 0;
                if (x_real < -0.02173)
                {
                    correction = x_real + 0.02173;
                    x_real = -0.02173;
                }
                header_stamp = finished_at_;
                x_val = x_real;
                time = time + intermediate_time + std::abs(correction);
            }
        } else {
            x_diff = calcSpotDiff(response_.Response.data[i].x, last_x);
            x_val = x_val - x_diff + intermediate_time;
            time = time + intermediate_time;
        }
        angles = calculateAngles(response_.Response.data[i].a,
                response_.Response.data[i].b, response_.Response.data[i].c);
        points = setCuttingPath(i, angles, x_val, time);
        cut_start = points[0];
        cut_end = points[1];
        placeholder.data.push_back(cut_start);
        placeholder.data.push_back(cut_end);
        last_x = response_.Response.data[i].x;
    }
    finished_at_ = header_stamp + ros::Duration(time);
    response_.Response.data = placeholder.data;
}

void Planner::rotateAboutZ(Eigen::Vector3f &cut_vector, const float &yaw)
{
    Eigen::Matrix3f rot_z; // Rotational matrix for rotation about Z-axis

    rot_z << cos(yaw), -sin(yaw), 0,
             sin(yaw),  cos(yaw), 0,
                 0   ,     0    , 1;

    cut_vector = rot_z * cut_vector;
}

float Planner::distanceFromWorkspace(const float &x_pos)
{
    return (std::abs(x_pos) - std::abs(x_ee_min_) + 0.1);
}

float Planner::calcIntermediateTime()
{
    return (alloted_travel_time_ / (response_.Response.data.size() - 1));
}

float Planner::calculateTotalTravelTime(const int &count)
{
    float cutting_time = count * cut_length_; /**< Cutting time for each spot, based in speed of 1m/s of conveyor */
    return (0.7 - cutting_time);
}

float Planner::calcSpotDiff(const float &current, const float &last)
{
    return (std::abs(current) - std::abs(last));
}

float Planner::remap(const float &des, const float &curr_min, const float &curr_max,
        const float &des_min, const float &des_max)
{
    float remapped = ((des - curr_min) / (curr_max - curr_min)) * (des_max - des_min) + des_min;
    return remapped;
}

float Planner::calculateMidpoint(const float &first, const float &second)
{
    float mid = (first + second) / 2; // Midpoint between two points
    return mid;
}

std::vector<custom_msgs::EulerAngles> Planner::setCuttingPath(const int &i, const std::vector<float> &angles,
        const float &x, float &t)
{
    std::vector<custom_msgs::EulerAngles> points; // Trajectory points in a cutting path
    Eigen::Vector3f cut = xyCut(angles[0], angles[1]); // Vector with x and y components for a given cut
    custom_msgs::EulerAngles start = response_.Response.data[i]; // Start position for a cut
    custom_msgs::EulerAngles end = start; // End position for a cut
    if (angles[0] > 0 && angles[1] > 0)
    {
        start.x = x + cut[0];
        start.y = response_.Response.data[i].y + cut[1];
    }
    else if (angles[0] < 0 && angles[1] > 0)
    {
        start.x = x + cut[0];
        start.y = response_.Response.data[i].y + cut[1];
    }
    else if (angles[0] < 0 && angles[1] < 0)
    {
        start.x = x - (cut[0]);
        start.y = response_.Response.data[i].y - cut[1];
    }
    else if (angles[0] > 0 && angles[1] < 0)
    {
        start.x = x - cut[0];
        start.y = response_.Response.data[i].y - cut[1];
    }
    end.x = x;
    end.y = response_.Response.data[i].y;
    start.waypoint_time = t;
    t = t + cut_length_;
    end.waypoint_time = t;
    points.push_back(start);
    points.push_back(end);

    return points;
}

Eigen::Vector3f Planner::xyCut(const float &yaw, const float &pitch)
{
    Eigen::Vector3f cut(cut_length_, 0, 0); // Initial vector for cut-length
    rotateAboutZ(cut, yaw);

    return cut;
}

std::vector<float> Planner::calculateAngles(const float &a, const float &b, const float &c)
{
    float phi = c; // Angle for yaw
    float theta; // Angle for pitch
    std::vector<float> angles; // Vector containing angles for yaw and pitch
    angles.resize(2);
    Eigen::Vector3f A_cross_B; // Cross product A x B
    Eigen::Matrix3f rot_z; // Rotational matrix for Z

    theta = M_PI - acos( pow( ( cos(a) * cos(b) ) /
                ( -pow(cos(a), 2) * pow(cos(b), 2) +
                  pow(cos(a), 2) + pow(cos(b), 2) ), 0.5 ) );
    if (theta > (M_PI / 2))
        theta = M_PI - theta;

    // Check the quadrant of c, 0 < c < 90
    if ((phi > 0) && (phi < (M_PI / 2)))
    {
        phi = phi + M_PI;
        // theta = -theta;
    }
    else if ((phi < (2 * M_PI)) && (phi > (3 * M_PI) / 2))
    {
        phi = phi - M_PI;
        // theta = -theta;
    }

    phi = remap(phi, (M_PI / 2), ((3 * M_PI) / 2), (-M_PI / 2), (M_PI / 2));

    angles[0] = phi;
    angles[1] = theta;

    return angles;
}

