#ifndef PLANNER_H
#define PLANNER_H

#include <ros/ros.h> /**< Header file for the roscpp implementation of ROS. Provides client library to interface with ROS topics, services and parameters */
#include <Eigen/Dense> /**< Eigen library that provides definitions for vectors and matrices used */
#include <custom_msgs/CameraOutput.h> /**< Custom message-definition header for CameraOutput.msg */
#include <custom_msgs/PlannerService.h> /**< Custom service-definition header for KinematicService.srv */

/**
 * \class Planner
 *
 * \brief Class responsible for path planning in the RoboTrim control system.
 *
 * Path is calculated based on the output from a camera node, which might be from
 * either a simulated environment or a physical environment. The path is based on
 * time given in ros::Time and provides a planner-service through a node that returns
 * an array with floats representing points in the path of the end-effector at any given
 * time for a sequence of spots.
 */
class Planner
{
    public:

        /**
         * \brief Standard constructor
         *
         * @param nh Pointer to NodeHandle object used in ROS
         */
        Planner(ros::NodeHandle* nh);

        ~Planner(){};

        /**
         * Callback function invoked when the PlannerService is called from the
         * controller. The callback expects a randomly sized array of camera output
         * variables consisting of XYZABC and returns a randomly sized array of
         * camera output variables based on the path generated.
         *
         * @param req PlannerService::Request received from client
         * @param res PlannerService::Response sent as a response to client
         */
        bool plannerServiceCallback(custom_msgs::PlannerService::Request &req,
                custom_msgs::PlannerService::Response &res);

        /**
         * \brief Sort the output from the camera in descending order based on X.
         *
         * Uses the sort function in the standard C++ library to sort the spot-data
         * contained in the response by X in ascending order. This is done in order
         * for the cutting sequence to go from high to low X, which means the knife
         * will finish the sequence closest to the next sequence of spots.
         */
        void sortPositions();

        /**
         * \brief Insert via-points for Z-coordinates between sorted positions.
         *
         * Inserts viapoints in the knife path for a given sequence of spots.
         * The viapoints include the area to cut and viapoints adjusting movement
         * in Z-axis between spots. The cut area is by default set to 25mm along
         * the direction in the XY-plane of the normal vector. The movement in Z-axis
         * is adjusted based on the highest spot in a given sequence, the knife should
         * move up 3mm from the highest placed spot between spots.
         */
        void assertViaPoints();

        /**
         * \brief Utility function to rotate a vector about the Z-axis.
         *
         * Utilizes the rotational matrix for rotation about Z
         * to rotate the vector the amount of degrees corresponding to the
         * value given by yaw.
         *
         * @param cut_vector Eigen::Vector3f that has the initial cut-length as X-component.
         * @param yaw float representing the angle to rotate the vector.
         */
        void rotateAboutZ(Eigen::Vector3f &cut_vector, const float &yaw);

        /**
         * \brief Calculates the distance from the workspace of RoboTrim.
         *
         * Calculates the distance between a detected/spawned spot and the
         * workspace of RoboTrim. This is further used to calculate the
         * difference in starting time for a cutting sequence if it differs
         * from the standard intermediate time.
         *
         * @param x_pos float with the value of X at spawn/detection time of a spot.
         *
         * @return Returns the distance a detected spot is from the
         * workspace as a float.
         */
        float distanceFromWorkspace(const float &x_pos);

        /**
         * \brief Calculate the intermediate time allowed in between spots.
         *
         * The intermediate time is the time allocated between each spot. This
         * time is dependent on the number of spots given by the request
         * and the total time allocated for processing the sequence of spots.
         *
         * @return The calculated intermediate time for a given sequence of spots.
         */
        float calcIntermediateTime();

        /**
         * \brief Calculate the time allocated for travelling for a fish.
         *
         * Calculates the time allocated for travelling between spots.
         * The cutting time depends on the number of spots and the cut
         * length of a spot.
         *
         * @param count Integer that gives the number of spots in a given sequence.
         */
        float calculateTotalTravelTime(const int &count);

        /**
         * \brief Calculate distance between spots in X-direction.
         *
         * The difference in spot position is found by subtracting the last
         * cut position in X-direction with the current position in X-direction.
         *
         * @param current The position of the current spot in X-direction.
         * @param last The position of the last cut spot in X-direction.
         */
        float calcSpotDiff(const float &current, const float &last);

        /**
         * \brief Remap values from one reference frame to another.
         *
         * @param des The value to remap from.
         * @param curr_min The minimum value in the reference frame to remap from.
         * @param curr_max The maximum value in the reference frame to remap from.
         * @param des_min The minimal value in the reference frame to remap to.
         * @param des_max The maximum value in the reference frame to remap from.
         *
         * @return remapped Float with the remapped value.
         */
        float remap(const float &des, const float &curr_min, const float &curr_max,
                const float &des_min, const float &des_max);

        /**
         * \brief Calculates the adjustment in cutting distance in terms of the different axis.
         *
         * Finds the cutting distance for each axis, calculated in terms of the angle given for yaw.
         * The initial cut-length is set as a value in the vector, the vector is then rotated
         * with the use a rotational matrix.
         *
         * @param yaw The angle for yaw.
         * @param pitch The angle for pitch.
         *
         * @return cut Eigen::Vector that gives the desired cut lengths in terms of x and y.
         */
        Eigen::Vector3f xyCut(const float &yaw, const float &pitch);

        /**
         * \brief Calculate the center between two spots
         *
         * Function used to calculate the middle-point used to assert viapoints to
         * calculated paths.
         *
         * @param first Value indicating the first point.
         * @param second Value indicating the second point.
         *
         * @return mid The point in the middle of the two given points.
         */
        float calculateMidpoint(const float &first, const float &second);

        /**
         * \brief Sets the cutting path for a spot.
         *
         * Calculates the cutting path for a specific spot in a sequence of spots.
         * Uses the function xyCut in order to calculate the cutting distances in
         * X- and Y-directions. The direction to cut is determined by the angles of
         * pitch and yaw. The cutting paths are different depending if the pitch is
         * negative or positive.
         *
         * @param i Index of the response.
         * @param angles Vector containing the angles for pitch and yaw.
         * @param x Indicates the position of a spots center in terms of the X-axis.
         * @param t Time variable that indicates the waypoint times of the previous spot.
         */
        std::vector<custom_msgs::EulerAngles> setCuttingPath(const int &i,
                const std::vector<float> &angles, const float &x, float &t);

        /**
         * \brief Calculates the angles of normals in the sequence of spots.
         *
         * The angle for pitch is given as theta, yaw as phi.
         * \f[
         * \theta = \pi - \arccos{\left( \sqrt{\frac{ \cos{a} \cdot \cos{b} }{ -\cos^2{a} \cdot \cos^2{b} + \cos^2{a} + \cos^2{b}}} \right)}
         * \f]
         *
         * Because RoboTrim has a area of movement that can rotate 180 degrees about the Z-axis,
         * theta is used to correct for the last 180 degrees to ensure the knife can cut
         * in an area of 360 degrees.
         *
         * In order to map from the world frame to the local reference frame, phi
         * is remapped.
         *
         * @param a The angle a gives the rotation about X. Part of xyzabc.
         * @param b The angle b gives the rotation about Y. Part of xyzabc.
         * @param c The angle c gives the rotation about Z. Part of xyzabc.
         *
         * @return angles Vector containing floats corresponding to theta and phi.
         */
        std::vector<float> calculateAngles(const float &a, const float &b, const float &c);

    private:

        ros::NodeHandle* node_ptr_;
        ros::ServiceServer planner_service_; /**< Variable to set up the planner service called from the controller.*/
        custom_msgs::PlannerService::Response response_; /**< Response type of the planner service */
        float alloted_travel_time_; /**< Time allowed for travelling between spots when handling a marker/fish */
        float last_x_; /**< Last x value in a given marker, used to calculate distance from previous to current marker */
        float cut_length_; /**< Default length to cut */
        float x_ee_min_; /**< Min value allowed for the end effector in reference to the global reference frame */
        int response_size_; /**< Indicates the number of points in the trajectory to plan */
        ros::Time finished_at_; /**< Time the planned trajectory is estimated to be finished relative to ROS time */
};

// end of class Planner
#endif
