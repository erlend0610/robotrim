cmake_minimum_required(VERSION 2.8.3)
project(rt_planner)

add_compile_options(-std=c++11)

find_package(Eigen3 REQUIRED)

if(NOT EIGEN3_INCLUDE_DIRS)
    set(EIGEN3_INCLUDE_DIRS ${EIGEN3_INCLUDE_DIR})
endif()

find_package(catkin REQUIRED COMPONENTS
    roscpp
    rospy
    std_msgs
    message_generation
    custom_msgs
)

catkin_package(
    CATKIN_DEPENDS message_runtime custom_msgs
    DEPENDS EIGEN3
)

include_directories(${EIGEN3_INCLUDE_DIRS})
include_directories(
    ${catkin_INCLUDE_DIRS}
)

add_executable(planner src/planner_main.cpp src/Planner.h src/Planner.cpp)
target_link_libraries(planner ${catkin_LIBRARIES})
add_dependencies(planner custom_msgs_generate_messages_cpp)
