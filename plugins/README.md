# plugins

Contains plugins used in the simulation environment.

#### Structure

* plugins/
    * src/
        * model_push.cpp
    * CMakeLists.txt
    * package.xml

##### src/model_push.cpp

Plugin that is used in the simulation to set velocity in X-direction to the spawned markers.
The value is set on each frame update in the simulation:

    this->model->SetLinearVel(math::Vector3(1.0, 0, 0));

The desired speed (m/s) can be adjusted in the `SetLinearVel(math::Vector3(1.0, 0, 0))` where the vector components correspond to X, Y, Z in cartesian space.

