# rt_model

Package containing the models for RoboTrim that is used in simulations. Also contains
the necessary launch files to be able to launch the model into RVIZ and Gazebo.

### Structure

* rt_model/
    * config/
        * robotrim_control_ii.yaml
        * robotrim_control.yaml
    * launch/
        * gazebo_kinect.launch
        * gazebo.launch
        * joints.launch
        * view_rt_rviz.launch
    * meshes/
        * base_link.STL
        * link_1.STL
        * link_2.STL
        * link_3.STL
        * link_4.STL
        * link_5.STL
    * urdf/
        * robotrim_MKII.generated.urdf
        * robotrim_MKII.urdf.xacro
    * CMakeLists.txt
    * config.rviz
    * package.xml

### Config files

#### robotrim_control_ii.yaml

YAML (YAML Ain't Markup Language) file that specifies the controllers used in the simulation.
Included in ../launch/joints.launch file in order to provide the interface between the controller and simulated
model.

**joint_state_controller**

Publishes all joint states from the simulated model to the `robotrim/JointStates` topic, where robotrim is the specified namespace. Provides real-time feedback of states of joints at a rate of 50 Hz.

**position_trajectory_controller**

Control interface to control the simulated model by position through the JointTrajectoryController. Provides the topic `robotrim/position_trajectory_controller/command` that is used by the controller to provide positional data to the simulation, can also be accessed through the command-line.

The position_trajectory_controller allows the specification of constraints, where the included constraints are `goal_time` which is set to 0.05 and `stopped_velocity_tolerance` which is set to 0.002.

The `goal_time` specifies the +/- allowed difference compared to the set goal time in order for a trajectory to be considered successful, given in s.

The `stopped_velocity_tolerance` specifies what velocity is considered as zero given in m/s.

Also provides individual topics for each joint (x, y, z, yaw, pitch)through the JointPositionController of position_controllers.

### Launch files

#### gazebo_kinect.launch

Standalone launch file for launching the URDF model in the Gazebo environment.

Launch arguments: `paused`, `use_sim_time`, `gui` `headless`, `debug`, `model` and `kinect`.

    paused // Start Gazebo in a paused state (default true)

    use_sim_time // ROS nodes asking for time gets the published simulation time from Gazebo from topic /clock (default true)

    gui // Launch the user interface window of Gazebo (default true)

    recording // Enable state log recording in Gazebo (default false)

    debug // Start Gazebo server in debug mode (default false)

    model // Pass the Xacro model of RoboTrim from rt_model package as argument

    kinect // Pass the model of the depth camera from rt_cam_mock as argument

Includes `rt_simulation.launch` from the rt_world package.

Launches a `robot_state_publisher` that publishes robot states to at a frequency of 30 Hz.

#### joints.launch

Wrapper launch file for launching the URDF model in the Gazebo environment together with setting up controllers specified in robotrim_control_ii.yaml

Launch arguments: `model`.

    model // Pass the Xacro model of RoboTrim from rt_model package as argument

Includes `gazebo_kinect.launch` specified above.

#### view_rt_rviz.launch

Launch file for launching the model into the Rviz (ROS Visualizer). The Rviz environment was mainly used earlier when testing different plugins for handling kinematics.

Launch arguments: `model`, `camera`, `kinect`, `use gui`

    model // Pass the Xacro model of RoboTrim from rt_model package as argument

    camera // URDF of a camera mock, unused

    kinect // URDF of a kinect-camera mock, unused

    use_gui // Launch the RVIZ user interface (default true)

Spawns nodes: `joint_state_publisher`, `robot_state_publisher`, `rviz`.

### Mesh files

Contains all meshes for the Xacro model. Imported from SolidWorks with the "Solidworks to URDF-exporter" plugin.

### URDF Files

Contains all URDF (Unified Robot Description Format) used in the simulated environments in RVIZ and Gazebo. URDF is a XML based format.

#### robotrim_MKII.generated.urdf

Generated raw URDF file auto generated through the xacro_to_urdf plugin in ROS from robotrim_MKII.urdf.xacro

#### robotrim_MKII.urdf.xacro

Xacro file based in the SolidWorks model of RoboTrim. Inherits properties specifying inertial
(inertial origin, mass, inertia), visuals (visual origin, geometry given by mesh, material)
and collision (origin, geometry given by mesh). Also specifies the joints in a SolidWorks
assembly These need custom specification when exporting the model from SolidWorks. Examples
of the definition of links and joints are given in the last paragraph.

Model also includes transmission blocks that allows the joints of a model in a Gazebo environment to act upon messages subscribed to by controllers.

Link specification example:

    <link name="base_link">
        <inertial>
            <origin xyz="0 0 0" rpy="0 0 0" />
            <mass value="47.174" />
            <inertia ixx="6.2709" ixy="-0.022112" ixz="0.013795" iyy="22.663" iyz="-1.1898" izz="20.519" />
        </inertial>
        <visual>
            <origin xyz="0 0 0" rpy="0 0 0" />
            <geometry>
                <mesh filename="package://rt_model/meshes/base_link.STL" />
            </geometry>
            <material name="">
                <color rgba="0.75294 0.75294 0.75294 1" />
            </material>
        </visual>
        <collision>
            <origin xyz="0 0 0" rpy="0 0 0" />
            <geometry>
                <mesh filename="package://rt_model/meshes/base_link.STL" />
			</geometry>
		</collision>
	</link>
	<gazebo reference="base_link">
		<material>Gazebo/Grey</material>
	</gazebo>

Joint specification example:

	<joint name="x_axis" type="prismatic">
		<origin xyz="0.79828 0.18895 0.1" rpy="1.5708 1.5708 0" />
		<parent link="link_1" />
		<child link="link_2" />
		<axis xyz="0 1 0" />
		<limit lower="-0.67148" upper="0.22852" effort="350" velocity="2.3" />
	</joint>

### Usage

To launch the simulated environment:

    cd ~/rt_ws

    roslaunch rt_model EXECUTABLE

where EXECUTABLE can be specified by:

    joints.launch

    view_rt_rviz.launch

    gazebo_kinect.launch
