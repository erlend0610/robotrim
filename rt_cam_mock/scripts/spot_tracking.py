#!/usr/bin/env python
from __future__ import print_function

import roslib
roslib.load_manifest('rt_cam_mock')
import sys
import rospy
import imutils
import cv2
import numpy as np

from imutils import contours
from skimage import measure
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


### @package rt_cam_mock
# Class containing functionalities used to detect spots given in spawned markers.
#
# Utilizes the OpenCV library together with CVBridge to integrate as a stand-alone
# ROS-node in the simulation environment. The images are captured through a subscriber
# and are further used in order to track red markers representing spots on the fish.
# Detection happens realtime and detects the center of each spot for easy reference.
class image_converter:

    cv_image = None
    red_mask = None
    hsv = None
    labels = None
    mask = None

    def __init__(self):
        ## Constructor
        #
        # Initializes CVBridge and sets the subscriber to listen to the image_raw topic
        # published from the Gazebo environment.
        #
        # @param self Object pointer
        self.bridge = CvBridge()
	self.image_sub = rospy.Subscriber("/camera/rgb/image_raw", Image, self.callback)

    def callback(self, data):
        ## Callback function that is called upon when the subscriber gets a return.
        #
        # Parses the image message from ROS to a useable blue green red format for OpenCV.
        # Sets the colormodel and defines the values used for red mask. Calls function in
        # order to track the contours of spots.
        #
        ## @param self Object pointer
        ## @param data The raw image data from the subscriber
        try:
            self.cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
	except CvBridgeError as e:
	    print(e)

	self.hsv = cv2.cvtColor(self.cv_image, cv2.COLOR_BGR2HSV)
	self.red_mask = self.define_red_mask()
	temp1 = self.contour_tracking(self.red_mask)

    def define_red_mask(self):
        ## Define the values used in order to mask red in the image.
        #
        ## @param self Object pointer
        ## @return mask Returns the mask with the values for red.
	lower_red = np.array([0, 50, 50])
	upper_red = np.array([10, 255, 255])
	mask0 = cv2.inRange(self.hsv, lower_red, upper_red)

	lower_red = np.array([170, 50, 50])
	upper_red = np.array([180, 255, 255])
	mask1 = cv2.inRange(self.hsv, lower_red, upper_red)
	mask = mask0 + mask1
	return mask

    def contour_tracking(self, mask):
        ## Tracks the contours of a detected spot to determine size and center.
        #
        ## @param self Object pointer
        ## @param mask The red mask
        self.labels = measure.label(mask, neighbors=8, background=0)
        self.mask = np.zeros(mask.shape, dtype="uint8")

        # Loop over the unique components in the given image
        for label in np.unique(self.labels):
            if label == 0:
                continue
            # Construct label mask and count the number of pixels
            labelMask = np.zeros(mask.shape, dtype="uint8")
	    labelMask[self.labels == label] = 255
	    numPixels = cv2.countNonZero(labelMask)

	contours_gen = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	contours_gen = contours_gen[0] if imutils.is_cv2() else contours_gen[1]
        if contours_gen.__len__() != 0:
	    contours_gen = contours.sort_contours(contours_gen)[0]

	for (i, c) in enumerate(contours_gen):
	    (x, y, w, h) = cv2.boundingRect(c)
	    ((cX, cY), radius) = cv2.minEnclosingCircle(c)
	    cv2.circle(self.cv_image, (int(cX), int(cY)), int(radius), (0, 0, 255), 3)

	cv2.imshow("Image window", self.cv_image)
        cv2.waitKey(3)

def main():
    ic = image_converter()
    rospy.init_node('image_converter', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
