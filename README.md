
RoboTrim        {#mainpage}
========

This repository is the software-part of the bachelor-project HS-RoboTrim 2018. The software contains 
the control system for RoboTrim, a 5DOF-Gantry style robot. It also contains a simulation used to test
and verify the functionality of the developed software in terms of the requirements given by the project
employer.

![Project structure](/images/PackageDiagram.png)

### Structure

***All packages have self contained README.md files.***

* RoboTrim/
    * custom_msgs/
    * plugins/
    * rt_controller/
    * rt_model/
    * rt_world/
    * rt_cam_mock/
    * rt_kinematics/
    * rt_planner/

All packages contain their own *CMakeLists.txt* and *package.xml*:

#### CMakeLists.txt

CMake file that provides the input to the CMake build system for building the specific package.
*For detailed information about CMake in catkin workspaces, see:*

http://wiki.ros.org/catkin/CMakeLists.txt

#### package.xml

Package manifest that must be included in the root of catkin-compliant packages. Defines properties
about the given package such as package name and dependencies on other catkin packages. *For
detailed information about the package manifest, see:*

http://wiki.ros.org/catkin/package.xml

### Quick start

#### Prerequisites

Prerequisites for the development environment and simulation.

*The software contained in the repository is developed and tested on a LENOVO YOGA720-131KB with
an Ubuntu Xenial 16.04 dual-boot configuration together with Windows 10.*

##### Robotic Operating System (ROS) Kinetic (supported distribution for Ubuntu 16.04)

The following guidelines for installation are the extracted necesseties for installing ROS Kinetic.
Full installation guide is given at http://wiki.ros.org/kinetic/Installation/Ubuntu.

*The software is not tested on other distributions of Ubuntu or ROS than 16.04 or Kinetic, if attempting
to install to other distros replace "kinetic" where necessary.*

##### Set up computer to accept software from packages.ros.org:

    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
	
##### Set up keys:

	sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
	
##### Make sure the Debian package is up to date:

	sudo apt-get update
	
##### Run full installation for ROS-Kinetic, (for alternative installation methods see full guide linked above):

	sudo apt-get install ros-kinetic-desktop-full

##### Initialize rosdep:

*Needed in order to use ROS and enable easy installation of system dependencies for source. Also required to run some
of the core components in ROS.*

	sudo rosdep init
	rosdep update
	
##### Add ROS environment variables automatically to every bash session:
	echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
	source ~/.bashrc

#### Installation

In order to be able to build the ROS-packages, a catkin workspace needs to be set up:

##### In ~:

	mkdir -p rt_ws/src
	
	cd src
	
	catkin_init_workspace
	
	cd ..
	
	catkin_make
	
##### Clone repository into /src of catkin workspace:

	git clone https://bitbucket.org/erlend0610/robotrim.git
	
#### Additional package requirements

The following packages are dependencies of the simulation in order to run:

##### Gazebo 7.0

Program used for the simulation.

	sudo apt-get install ros-kinetic-gazebo-ros-pkgs ros-kinetic-gazebo-ros-control

##### JointStateController

	sudo apt-get install ros-kinetic-joint-state-controller

##### EffortControllers

	sudo apt-get install ros-kinetic-effort-controllers

##### PositionControllers

	sudo apt-get install ros-kinetic-position-controllers

##### JointTrajectoryController

	sudo apt-get install ros-kinetic-joint-trajectory-controller

## Running simulation environment

The simulation is run from the **rt_model** package.

##### Ensure sourced environment

	cd ~/rt_ws
	source devel/setup/bash
	
##### Run simulation from rt_model/launch

	cd ~/rt_ws
	roslaunch rt_model joints.launch

##### Run controller node

    rosrun rt_controller controller

##### Run the planner node

    rosrun rt_planner planner

##### Run kinematics node

    rosrun rt_kinematics kinematic

##### Run object generator in Gazebo

    rosrun rt_world fish_generator
