#ifndef KINEMATIC_HANDLER_H
#define KINEMATIC_HANDLER_H

#include <ros/ros.h> /**< Header file for the roscpp implementation of ROS. Provides client library to interface with ROS topics, services and parameters */
#include <Eigen/Dense> /**< Eigen library that provides definitions for vectors and matrices used */
#include <custom_msgs/CameraOutput.h> /**< Custom message-definition header for CameraOutput.msg */
#include <custom_msgs/KinematicService.h> /**< Custom service-definition header for KinematicService.srv */

/**
 * \class KinematicHandler
 *
 * \brief Class responsible for generating trajectory points/kinematics.
 *
 * The kinematic node is responsible for calculating trajectory points given as x, y, z, yaw and pitch
 * based on requests from the controller class. The request is given in form of xyzabc which represents
 * xyz for cartesian coordinates and abc for rotation about the axis. When called the service invokes
 * the kinematicServiceCallback.
 *
 * Calculates trajectory points given as x, y, z, yaw and pitch based on request
 * from the controller class. The request is given in form of xyzabc which represents
 * xyz for cartesian coordinates and abc for rotation about the axis.
 */
class KinematicHandler
{
    public:
        KinematicHandler(ros::NodeHandle* nh);

        ~KinematicHandler(){};


        /**
         * \brief Callback function used in the kinematic service
         *
         * Handles the request sent from the controller containing a CameraOutput request.
         * The request represents desired positions for the knife. The callback calculates
         * joint configurations for the joints. The positions are sent as a response back
         * to the controller.
         *
         * @param req KinematicService::Request object received from client
         * @param res KinematicService::Response sent as a response to client
         */
        bool kinematicServiceCallback(custom_msgs::KinematicService::Request &req,
                custom_msgs::KinematicService::Response &res);

        /**
         * \brief Calculate the desired values for pitch and yaw given the rotations
         * about the xyz axis as abc.
         *
         * The rotations about xyz can be seen as roll, pitch and yaw. Since RoboTrim
         * is a 5DOF system with a 2DOF end-effector only able to handle yaw and pitch,
         * the rotations given from the detection system need to be recalculated from
         * three rotations to two. The vectors A and B are defined from the rotation
         * about the axises x and y where:
         *
         *      A = [ 0 , cos(a) , sin(a) ]
         *      B = [ cos(b) , 0 , sin(b) ]
         *
         * The two vectors form a plane which give the angle psi, that is the angle
         * between the globally defined z-axis and the plane, in other words the
         * angle for the pitch.
         *
         *
         *                                  cos(a)cos(b)
         *      psi = pi - acos( ----------------------------------- )^(1/2)
         *                        -cos²(a)cos²(b) + cos²(a) + cos²(b)
         *
         * Where the normal vector of the plane defined by A and B is given by:
         *
         *      n = A x B = [ cos(a)sin(b) , cos(b)sin(a) , -cos(a)cos(b) ]
         *
         * With the assumption that:
         *
         *      0 < a , b < 0.615478885
         *
         * in radians, psi, given as the angle between the global XY-plane and n will
         * be in the defined space of the pitch:
         *
         *      0 < psi < pi / 4
         *
         * The rotation about the z-axis is applied through the rotational matrix for z:
         *
         *
         *              | cos(c) -sin(c) 0 | | n_x |   | n_xcos(c) - n_ysin(c) |   | n_x' |
         *      Rot_Z = | sin(c)  cos(c) 0 | | n_y | = | n_xsin(c) + n_ycos(c) | = | n_y' |
         *              |   0       0    1 | | n_z |   |          n_z          |   | n_z' |
         *
         * where n_z, n_y, n_z are the components of the normal vector n.
         *
         * @param a float representing rotation about X
         * @param b float representing rotation about Y
         * @param c float representing rotation about Z
         *
         * @return angles std::vector of two floats representing angles for yaw and pitch
         */
        std::vector<float> calculateAngles(const float &a, const float &b, float &c);

        /**
         * \brief Calculate the correction to X, Y and Z joints.
         *
         * Calculates the correction needed to the X, Y and Z joints based on the rotation
         * of yaw and pitch. Acts as a wrapper for calcYawCorrection and calcPitchCorrection.
         * Uses a vector based on the dimensions of the knife to determine the offsets
         * with trigonometry.
         *
         * @param a float representing rotation about X
         * @param b float representing rotation about Y
         * @param c float representing rotation about Z
         *
         * @return rotation Eigen::Vector representing the rotated tool with components [x, y, z]
         */
        Eigen::Vector3f calcRotationalCorrection(const float &yaw, const float &pitch);

        /**
         * \brief Calculate the correction from yaw.
         *
         * Calculates the correction needed to the cartesian joints based on the rotation of
         * the yaw joint.
         *
         * @param yaw float representing angle for yaw
         * @param tool Eigen::Vector3f representing the tool with components [x, y, z]
         *
         * @return Rz*tool The rotated vector representing the tool with components [x, y z]
         */
        Eigen::Vector3f calcYawCorrection(const float &yaw, const Eigen::Vector3f &tool);

        /**
         * \brief Calculate the correction from pitch.
         *
         * Calculates the correction needed to the cartesian joints based on the rotation of
         * the pitch joint.
         *
         * @param pitch float representing angle for yaw
         * @param tool Eigen::Vector3f representing the tool with components [x, y, z]
         *
         * @return Ry*tool The rotated vector representing the tool with components [x, y z]
         */
        Eigen::Vector3f calcPitchCorrection(const float &pitch, const Eigen::Vector3f &tool);

        /**
         * \brief Remap values from one range of values to another.
         *
         * Used in order to map values from one reference frame to another, i.e from the knife's
         * local reference frame to the global, or opposite. This is needed in order to translate
         * coordinates received in terms of the global reference frame into joint configurations
         * given in local reference frames.
         *
         * @param ee_desired The desired position of the end-effector given in global reference
         * @param ee_min The minimum allowed position of the end-effector given in global reference
         * @param ee_max The maximum allowed position of the end-effector given in global reference
         * @param local_min The minimum allowed position of the joint given in local reference
         * @param local_max The maximum allowed position of the joint given in local reference
         *
         * @return remapped float with the remapped value
         */
        float remapValues(const float &ee_desired, const float &ee_min, const float &ee_max,
                const float &local_min, const float &local_max);


    private:

        ros::NodeHandle* node_ptr_;
        ros::ServiceServer kinematic_service_; /**< Service-server for the kinematic service. */

        /**
         * These values are used to map the position of the end-effector to
         * alternative coordinate frames if needed. Mostly needed for the simulated
         * environment where SolidWorks tend to generate random origins which are to much of a
         * hassle to deal with in URDF at every new import.
         *
         * Variables denoted with 'ee' are min/max positions of the origin of the end-effector
         * in terms of the world frame. Variables denoted 'local' are min/max positions of each
         * axis in terms of the locally set reference frame.
         *
         * If the coordinate frame matches like in a real world scenario, the min/max values of
         * ee/local will match hence give a 1:1 relationship.
         */
        float X_ee_min_, X_ee_max_, Y_ee_min_, Y_ee_max_, Z_ee_min_, Z_ee_max_,
              X_local_min_, X_local_max_, Y_local_min_, Y_local_max_,
              Z_local_min_, Z_local_max_;

        float knife_z_, knife_x_inner_, knife_x_outer_; /**< Dimensions for the knife used in vector */

        ros::Time last_unique_stamp_; /**< Timestamp variable used in the wrapper to check which requests are valid */
};

// end of class KinematicHandler
#endif
