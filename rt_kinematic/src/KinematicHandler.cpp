#include "KinematicHandler.h"
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <custom_msgs/JointConfiguration.h>
#include <custom_msgs/KinematicService.h>

KinematicHandler::KinematicHandler(ros::NodeHandle* nh)
{
    node_ptr_ = nh;

    // Set up service for the kinematics
    kinematic_service_ = node_ptr_ -> advertiseService("/robotrim/kinematic_service", &KinematicHandler::kinematicServiceCallback, this);

    // Specify min/max positional values for the end-effector in terms of the global reference frame
    X_ee_min_ = -0.020730;
    X_ee_max_ = 0.879209;
    Y_ee_min_ = -0.773849;
    Y_ee_max_ = -1.173831;
    Z_ee_min_ = 0.762857;
    Z_ee_max_ = 0.860857;

    // Specify the min/max values in terms of the local reference frames of the cartesian joints
    X_local_min_ = -0.67148;
    X_local_max_ = 0.22852;
    Y_local_min_ = -0.1365;
    Y_local_max_ = 0.2635;
    Z_local_min_ = 0;
    Z_local_max_ = 0.098;

    knife_z_ = 0.1377821794;
    knife_x_inner_ = 0.0425;
    knife_x_outer_ = 0.0855;
}

bool KinematicHandler::kinematicServiceCallback(custom_msgs::KinematicService::Request &req,
        custom_msgs::KinematicService::Response &res)
{
    float x_val, y_val, z_val, yaw_val, pitch_val;
    float x_diff = x_val - X_ee_min_;
    float x_diff_placeholder;
    trajectory_msgs::JointTrajectory trajectory;
    trajectory_msgs::JointTrajectoryPoint x, y, z, yaw, pitch;
    custom_msgs::JointConfiguration joints;
    std::vector<float> angles;
    angles.resize(2);

    if (req.Request.header.stamp != last_unique_stamp_)
    {
        ros::Duration waypoint_time;
        ros::Duration increment;
        res.Response.joint_names.resize(5);
        res.Response.points.resize(req.Request.data.size());

        for (int i = 0; i < res.Response.points.size(); ++i)
        {
            res.Response.points[i].positions.resize(5);
        }

        /**
         * Assign joint names to the response. Position data corresponding
         * to the joints needs to be ordered in the same way in the response.
         */
        res.Response.header.stamp = req.Request.header.stamp;
        res.Response.joint_names[0] = "x_axis";
        res.Response.joint_names[1] = "y_axis";
        res.Response.joint_names[2] = "z_axis";
        res.Response.joint_names[3] = "yaw";
        res.Response.joint_names[4] = "pitch";

        // Calculate the kinematic solution for all entries in the request
        for (int i = 0; i < req.Request.data.size(); ++i)
        {
            x_val = remapValues(req.Request.data[i].x, X_ee_min_, X_ee_max_, X_local_min_, X_local_max_);
            y_val = remapValues(req.Request.data[i].y, Y_ee_min_, Y_ee_max_, Y_local_min_, Y_local_max_);
            z_val = remapValues(req.Request.data[i].z, Z_ee_min_, Z_ee_max_, Z_local_min_, Z_local_max_);
            angles = calculateAngles(req.Request.data[i].a, req.Request.data[i].b, req.Request.data[i].c);

            yaw_val = angles[0];
            pitch_val = angles[1];

            Eigen::Vector3f R = calcRotationalCorrection(yaw_val, pitch_val);
            waypoint_time = ros::Duration(req.Request.data[i].waypoint_time);

            if (i == req.Request.data.size() - 1)
                res.Response.points[i].positions[0] = x_val;
            else
                res.Response.points[i].positions[0] = x_val;

            res.Response.points[i].positions[1] = y_val + R[1];
            res.Response.points[i].positions[2] = z_val + R[2];
            res.Response.points[i].positions[3] = yaw_val;
            res.Response.points[i].positions[4] = pitch_val;
            res.Response.points[i].time_from_start = waypoint_time;
        }
        last_unique_stamp_ = req.Request.header.stamp;
    }
}

std::vector<float> KinematicHandler::calculateAngles(const float &a, const float &b, float &c)
{
    // Vector used as return containing angles for pitch and yaw
    std::vector<float> angles;
    angles.resize(2);
    float psi;
    Eigen::Vector3f A_cross_B;
    Eigen::Vector3f n;
    Eigen::Matrix3f rot_z;

    // Angle for pitch
    psi = M_PI - acos(pow((cos(a) * cos(b)) /
                (-pow(cos(a), 2) * pow(cos(b), 2) +
                  pow(cos(a), 2) + pow(cos(b), 2)), 2 ));

    if (psi > (M_PI / 2))
        psi = M_PI - psi;

    // Normal-vector from the rotations a, b about x, y
    A_cross_B = Eigen::Vector3f(cos(a) * sin(b) , cos(b) * sin(a) , -cos(a) * cos(b));

    rot_z << cos(c), -sin(c), 0,
             sin(c),  cos(c), 0,
               0   ,    0   , 1;

    // Multiplying the vector resultant from A x B with the rotational matrix for rotation about Z.
    n = rot_z * A_cross_B;

    // Check the quadrant of c
    // 0 < c < 90
    if ((c > 0) && (c < (M_PI / 2)))
    {
        c = c + M_PI;
        // psi = -psi;
    }
    // 270 < c 360
    else if ((c < (2 * M_PI)) && (c > (3 * M_PI) / 2))
    {
        c = c - M_PI;
        // psi = -psi;
    }

    c = remapValues(c, (M_PI / 2), ((3 * M_PI) / 2), (-M_PI / 2), (M_PI / 2));

    angles[0] = c;
    angles[1] = psi;

    return angles;
}

Eigen::Vector3f KinematicHandler::calcRotationalCorrection(const float &yaw, const float &pitch)
{
    // Vector variable for defining the dimensions of the tool as a vector
    Eigen::Vector3f tool;

    // Vector variable for defining the trigonometric dimensions of the tool vector when rotated
    Eigen::Vector3f rotation;

    // Assign the measurements to the tool vector
    tool = Eigen::Vector3f(knife_x_inner_, 0.0, knife_z_);

    // Check angles
    if ((yaw < 0 && pitch < 0) || (yaw > 0 && pitch < 0))
    {
        // Calculate correction due to the rotations of yaw and pitch
        rotation = calcYawCorrection(yaw, tool);
        rotation = calcPitchCorrection(pitch, rotation);

        // Correct the Z-value
        rotation[2] = knife_z_ - std::abs(knife_z_ - rotation[2]);
    } else if ((yaw > 0 && pitch > 0) || (yaw < 0 && pitch > 0))
    {
        // Calculate correction due to the rotations of yaw and pitch
        rotation = calcYawCorrection(yaw, tool);
        rotation = calcPitchCorrection(pitch, rotation);

        // Correct the Z-value
        rotation[2] = knife_z_ + std::abs(knife_z_ - rotation[2]);
    }

    return rotation;
}

Eigen::Vector3f KinematicHandler::calcYawCorrection(const float &yaw, const Eigen::Vector3f &tool)
{
    Eigen::Matrix3f Rz;

    // Matrix for rotation about the Z-axis
    Rz << cos(yaw), -sin(yaw), 0,
          sin(yaw),  cos(yaw), 0,
             0    ,     0    , 1;

    return Rz * tool;
}

Eigen::Vector3f KinematicHandler::calcPitchCorrection(const float &pitch, const Eigen::Vector3f &tool)
{
    Eigen::Matrix3f Ry;

    // Matrix for rotation about the Y-axis
    Ry <<  cos(pitch), 0, sin(pitch),
               0     , 1,      0    ,
          -sin(pitch), 0, cos(pitch);

    return Ry * tool;
}

float KinematicHandler::remapValues(const float &ee_desired, const float &ee_min, const float &ee_max,
        const float &local_min, const float &local_max)
{
    float remapped = ((ee_desired - ee_min) / (ee_max - ee_min)) * (local_max - local_min) + local_min;
    return remapped;
}
