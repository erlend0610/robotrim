#include "KinematicHandler.h"

/**
 * Runs the kinematic service
 */
int main(int argc, char** argv)
{
    ros::init(argc, argv, "rt_kinematics");
    ros::NodeHandle nh;
    KinematicHandler kinematic(&nh);

    while(ros::ok())
    {
        ros::spin();
    }
}
