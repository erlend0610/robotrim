# rt_kinematic

Package that contains the service-node responsible for handling the kinematics in the software.
Interfaces with the *controller* node from the rt_controller package by providing a service
specified by KinematicService.srv. The service takes requests in the form of CameraOutput.msg
and sends a response in the form of JointTrajectory.msg.

### Structure

* rt_kinematic/
    * src/
        * KinematicHandler.cpp
        * KinematicHandler.h
        * kinematic_main.cpp
    * CMakeLists.txt
    * package.xml

#### src/KinematicHandler.cpp

The calculated kinematics are based in the measurements of the axes of the imported
SolidWorks model. The current model is v2, as the third and final version did not
get finished in time for import to the simulation environment.

The measurements are given by `X_ee_min_` to `Z_ee_max_` and `X_local_min_` to
`Z_local_max_`. Also the measurements of the end-effector/knife is given by
`knife_z_`, `knife_x_inner_`, `knife_x_outer_`.

If the model is changed these need to be changed to corresponding values in order
to calculate the correct kinematic solutions for the model.

#### src/KinematicHandler.h

Standard header file

#### src/kinematic_main.cpp

Specified as the executable file in *CMakeLists.txt*. Runs the kinematics-node to
provide the service specified in KinematicHandler.cpp.

### Usage

To run as a separate node:

###### Navigate to the catkin workspace

    cd ~/rt_ws

###### Run the node with `rosrun`:

    rosrun rt_kinematic kinematics

